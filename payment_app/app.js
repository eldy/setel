const express = require('express')
const app = express()
const port = 3001

app.post('/payment', (req, res) => {
	res.json({
		verified: Math.random() < 0.5
	})
})

app.listen(port, () => console.log(`app listening on port ${port}!`))
