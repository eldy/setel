import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import axios from 'axios'

const Flex = styled.div`
	width: 400px;
	display: flex;
	justify-content: space-between;
`

function App() {
	const [orders, setOrders] = useState([])
	const [price, setPrice] = useState('')
	const [cancelOrderId, setCancelOrderId] = useState('')

	const [apiStatus, setApiStatus] = useState(true)

	useEffect(() => {
		const fetchOrders = async () => {
			const {
				data: { orders }
			} = await axios.get('http://localhost:3000/api/orders')

			setOrders(orders)
		}
		fetchOrders()

		const monitorApiStatus = async () => {
			setInterval(async () => {
				try {
					const { data } = await axios.get('http://localhost:3000/api/health')
					data === 'ok' ? setApiStatus(true) : setApiStatus(false)
				} catch (e) {
					setApiStatus(false)
				}
			}, 1000)
		}
		monitorApiStatus()
	}, [])

	async function handlePlaceOrder() {
		const { data } = await axios.post('http://localhost:3000/api/order', {
			price
		})

		setOrders([data, ...orders])
		setPrice('')
	}

	async function handleCancelOrder() {
		const {
			data: { _id, status }
		} = await axios.put('http://localhost:3000/api/order/cancel', {
			id: cancelOrderId
		})

		const newOrders = orders.map(o =>
			o._id === _id ? { ...o, _id, status } : o
		)
		setOrders(newOrders)
		setCancelOrderId('')
	}

	return (
		<Flex>
			<div>
				<h2>Order History</h2>
				<div>
					{orders &&
						orders.map(o => (
							<div key={o._id}>
								<div>Id: {o._id}</div>
								<div>
									{' '}
									<strong>${o.price}</strong> - <strong>{o.status}</strong>
								</div>
							</div>
						))}
				</div>
			</div>

			<div>
				<h2>Create order</h2>
				<div>
					<input
						onChange={e => setPrice(e.target.value)}
						type="number"
						placeholder="price"
						value={price}
					/>
					<button disabled={!price} onClick={handlePlaceOrder}>
						Place Order
					</button>
				</div>
			</div>

			<div>
				<h2>Cancel order</h2>
				<div>
					<input
						onChange={e => setCancelOrderId(e.target.value)}
						type="text"
						placeholder="order id"
						value={cancelOrderId}
					/>
					<button disabled={!cancelOrderId} onClick={handleCancelOrder}>
						Cancel Order
					</button>
				</div>
			</div>

			<div>
				<h2>API status</h2>
				<div>{apiStatus ? 'Healthy' : 'Down'}</div>
				<div />
			</div>
		</Flex>
	)
}

export default App
