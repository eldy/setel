import mongoose from 'mongoose'

const orderSchema = new mongoose.Schema({
  price: Number,
  status: {
    type: String,
    enum: ['created', 'delivered', 'cancelled'],
    default: 'created'
  }
})

export default mongoose.model('Order', orderSchema)
