import 'dotenv/config'
import cors from 'cors'
import bodyParser from 'body-parser'
import express from 'express'
import mongoose from 'mongoose'
import errorHandlers from './handlers/errorHandler'

const connectDb = () => {
  return mongoose.connect(process.env.DATABASE_URL, {
    useCreateIndex: true,
    useNewUrlParser: true
  })
}

import routes from './routes'

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// Routes
app.use('/api', routes)

connectDb()
  .then(async () => {
    app.listen(process.env.PORT, () =>
      console.log(`app listening on port ${process.env.PORT}!`)
    )
  })
  .catch(e => {
    console.log(e)
  })

if (app.get('env') === 'dev') {
  app.use(errorHandlers.developmentErrors)
}

app.use(errorHandlers.productionErrors)
