import { Router } from 'express'
import { catchErrors } from '../handlers/errorHandler'
const router = Router()

import {
  initOrder,
  completeOrder,
  cancelOrder,
  getOrders,
  getOrderStatus
} from '../controllers/orderController'
import { verifyPayment } from '../controllers/paymentController'

router.get('/orders', getOrders)
router.post(
  '/order',
  catchErrors(initOrder),
  catchErrors(verifyPayment),
  catchErrors(completeOrder)
)
router.put('/order/cancel', catchErrors(cancelOrder))
router.get('/order/:id', getOrderStatus)

router.get('/health', (req, res) => res.send('ok'))

export default router
