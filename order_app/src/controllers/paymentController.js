import axios from 'axios'

exports.verifyPayment = async (req, res, next) => {
  const {
    data: { verified }
  } = await axios.post('http://localhost:3001/payment')

  res.locals.verified = verified

  next()
}
