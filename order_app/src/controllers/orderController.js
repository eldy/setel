import Order from '../models/Order'

exports.getOrders = async (req, res, next) => {
  const data = await Order.find({}, null, {
    sort: { _id: -1 }
  })

  res.json({
    orders: data
  })
}

exports.initOrder = async (req, res, next) => {
  const { price } = req.body
  const { _id } = await new Order({
    price
  }).save()

  res.locals = {
    orderId: _id,
    price
  }

  next()
}

exports.completeOrder = async (req, res, next) => {
  const { orderId, price, verified } = res.locals
  const orderStatus = verified ? 'delivered' : 'cancelled'

  const { _id, status } = await Order.findOneAndUpdate(
    { _id: orderId },
    { status: orderStatus },
    { new: true }
  )

  res.json({
    _id,
    status,
    price
  })
}

exports.getOrderStatus = async (req, res) => {
  const { id } = req.body
  const { _id, status } = await Order.findOne({ id })

  res.json({
    _id,
    status
  })
}

exports.cancelOrder = async (req, res) => {
  const { id } = req.body

  const { _id, status } = await Order.findOneAndUpdate(
    { _id: id },
    { status: 'cancelled' },
    { new: true }
  )

  res.json({
    _id,
    status
  })
}
